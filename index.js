const MAX_WORD_LENGTH = 10;
const arr = ['a.oibcdef', 'bcd', 'cde', '123', -1];

function cleanArr(arr) {
    return arr.filter(e => e.length);
}

function getOtherOccMap(arr, letter) {
    const map = {}
    for (let arrIdx = 0; arrIdx < arr.length; arrIdx++) {
        const word = arr[arrIdx];
        if (word.indexOf(letter) > -1) {
            for (let letterIdx = 0; letterIdx < word.length; letterIdx++) {
                if (letter !== word[letterIdx]) {
                    if (!map[word[letterIdx]]) {
                        map[word[letterIdx]] = 1;
                    } else {
                        map[word[letterIdx]]++;
                    }
                }
            }
        }
    }
    return map;
}

function getLetters(arr) {
    const letters = [];
    for (let wordIdx = 0; wordIdx < arr.length; wordIdx++) {
        const word = arr[wordIdx];
        for (let letterIdx = 0; letterIdx < word.length; letterIdx++) {
            if (letters.indexOf(word[letterIdx]) < 0) {
                letters.push(word[letterIdx]);
            }
        }
    }
    return letters;
}

function getMaxForLetter(arr, letter) {
    const map = getOtherOccMap(arr, letter);
    let max = -1;
    let maxKeys = [];

    for (const key of Object.keys(map)) {
        if (max < map[key]) {
            max = map[key];
        }
    }

    for (const key of Object.keys(map)) {
        if (max === map[key]) {
            maxKeys.push(key);
        }
    }

    return maxKeys.join(',');
}

function getOutput(arr) {
    const clean = cleanArr(arr);

    const letters = getLetters(clean);
    const result = [];

    for (const letter of letters) {
        result.push(letter + ': ' + getMaxForLetter(clean, letter));
    }

    return result.join('\n');
}



console.log(getOutput(arr));